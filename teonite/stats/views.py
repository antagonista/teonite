from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from teonite.stats.serializers import StatSerializer
from teonite.stats.models import StatsRow
from rest_framework.response import Response



class StatsViewSet(viewsets.ModelViewSet):

    queryset = StatsRow.objects.all()
    serializer_class = StatSerializer

    lookup_field = 'author'
    lookup_value_regex = '[a-zżźćńółęąś]+'


    def initial(self, request, *args, **kwargs):

        # changes

        StatsRow.objects.all().delete()  # cleaning after the last request

        result = None  # initiation of a variable

        if kwargs:
            # checking that there are arguments
            # execution of the query
            result = StatsRow.objects.raw('''SELECT word as id, nentry FROM ts_stat('SELECT to_tsvector(array_to_string(array(SELECT content FROM postblog_postblog WHERE author = ''' + "''" + kwargs['author'] + "''" + '''), '' ''))') ORDER BY nentry DESC LIMIT 10;''')

        else:
            # there are no arguments
            # execution of the query
            result = StatsRow.objects.raw(
                '''SELECT word as id, nentry FROM ts_stat('SELECT to_tsvector(array_to_string(array(SELECT content FROM postblog_postblog), '' ''))') ORDER BY nentry DESC LIMIT 10;''')

        if result is not None:
            # checking: result is not empty

            for row in result:
                # processing of elements

                if kwargs:
                    # checking that there are arguments
                    # initiating objects with the author -> /stats/<author>/
                    StatsRow.objects.create(word=row.id, nentry=row.nentry, author=kwargs['author'])

                else:
                    # there are no arguments
                    # initiating objects without the author -> /stats/
                    StatsRow.objects.create(word=row.id, nentry=row.nentry)

        # default

        self.format_kwarg = self.get_format_suffix(**kwargs)

        # Ensure that the incoming request is permitted
        self.perform_authentication(request)
        self.check_permissions(request)
        self.check_throttles(request)

        # Perform content negotiation and store the accepted info on the request
        neg = self.perform_content_negotiation(request)
        request.accepted_renderer, request.accepted_media_type = neg

        # Determine the API version, if versioning is in use.
        version, scheme = self.determine_version(request, *args, **kwargs)
        request.version, request.versioning_scheme = version, scheme

    def retrieve(self, request, author=None):
        # GET - Show /stats/<author>/

        return self.make_output()

    def list(self, request, *args, **kwargs):
        # GET - Show /stats/

        return self.make_output()

    def make_output(self):
        # default

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        # changes

        if len(serializer.data) is not 0:
            # check that it is not empty
            json_output = {}  # initialize an output variable

            for element in serializer.data:
                # processing of components
                json_output[element[0]] = element[1]  # adding an element to the output variable

            # return the output variable
            return Response(json_output)

        else:
            # is empty - return the default value
            return Response(serializer.data)
