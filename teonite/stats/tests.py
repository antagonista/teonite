# -*- coding: utf-8 -*-

from django.test import TestCase, Client
from teonite.stats.models import StatsRow
import sys
#sys.stderr.write(repr(object_to_print) + '\n')


class StatsTests(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = 'http://localhost:8080/stats/'

    def test_stats_status_code(self):

        response = self.client.get(self.url)
        assert response.status_code == 200


class StatsTestsAuthor(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = 'http://localhost:8080/stats/kamilchudy/'

    def test_stats_author_status_code(self):

        response = self.client.get(self.url)
        assert response.status_code == 200














