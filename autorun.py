import os

os.system("python3 manage.py makemigrations")

os.system("python3 manage.py migrate")

os.system("psql -h db -U postgres -d postgres -a -f init_structure.sql")

os.system("cd /code/scrapy_teonite/ && scrapy crawl teonite_blog")

os.system("python3 manage.py runserver 0.0.0.0:8080")


