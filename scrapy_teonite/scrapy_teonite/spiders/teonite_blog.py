# -*- coding: utf-8 -*-
import scrapy
from scrapy_teonite.items import *


class TeoniteBlogSpider(scrapy.Spider):
    # class of Spider
    name = 'teonite_blog'  # name of spider
    start_urls = ['http://build.sh/']  # starting point

    def parse(self, response):
        # main main parsing method

        for postHref in response.css('a.read-more'):
            # link searches for posts and follow to site
            yield response.follow(postHref, self.parse_post)  # follow to post site and parsing

        for next_page in response.css('a.older-posts'):
            # link searches for older posts and follow to
            yield response.follow(next_page, self.parse)  # follow to older post site

    def parse_post(self, response):
        # parsing blog post method

        item = PostItem()  # initiation PostItem object
        # separating and formatting the author of the post
        item['author'] = ((response.css('span.author-content h4::text').extract_first()).replace(' ', '')).lower()
        # separating and formatting the content of the post
        item['content'] = " ".join((' '.join(response.css('section.post-content ::text').extract())).strip().split())

        yield item  # return result



