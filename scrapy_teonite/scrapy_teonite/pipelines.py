# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

import psycopg2
from scrapy_teonite.items import *


class ScrapyTeonitePipeline(object):

    def __init__(self):
        # initiation class - connect to DB
        self.connection = psycopg2.connect(host='db', database='postgres', user='postgres')
        self.cursor = self.connection.cursor()

    def process_item(self, item, spider):
        # check item type

        try:
            if type(item) is PostItem:
                # processing item Post

                # insert to postblog table
                self.cursor.execute("""SELECT * FROM insert_into_postblog(%s,%s)""",(item.get('author'),item.get('content')))

            # confirmation query
            self.connection.commit()
            self.cursor.fetchall()

        except ValueError:
            print("ERROR: insert to database")

        return item