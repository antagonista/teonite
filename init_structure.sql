-- function to add new records without repeating them

CREATE OR REPLACE FUNCTION insert_into_postblog(newauthor text, newcontent text)
returns bool as
$$
DECLARE
	count_row integer;

BEGIN

SELECT INTO count_row COUNT(content) FROM postblog_postblog WHERE author = newauthor AND content = newcontent;

	IF count_row = 0 THEN
		INSERT INTO postblog_postblog (author, content) VALUES (newauthor,newcontent);
		RETURN TRUE;
	ELSE RETURN FALSE;
	END IF;

END;
$$
language plpgsql;



-- function to return a list of 10 most common words with frequency statistics

CREATE OR REPLACE FUNCTION get_frequency_words(query text)
RETURNS text[] AS
$$
BEGIN

	RETURN array_agg(t.result) FROM (SELECT ARRAY[word, nentry::text] as result FROM ts_stat('SELECT to_tsvector(array_to_string(array(' || query || '), '' ''))') ORDER BY nentry DESC LIMIT 10) as t;

END;
$$
language plpgsql;
