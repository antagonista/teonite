FROM python:3.6

RUN apt-get update && apt-get install python3-psycopg2
RUN apt-get -y install postgresql postgresql-contrib

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

ADD requirements.txt /code/
ADD wait-for-postgres.sh /code/
ADD init_structure.sql /code/
ADD autorun.py /code/

RUN pip3 install -r requirements.txt
RUN pip install scrapy

ADD crontab /etc/cron.d/scrapycron
RUN chmod 0644 /etc/cron.d/scrapycron
RUN touch /var/log/cron.log
CMD cron && tail -f /var/log/cron.log

ADD . /code/